function String2(arg2){
    let repStr=arg2.split('.').join(" ").split(" ");
    if(repStr.length !==4){
        return [];
    }
    for (let i=0;i<repStr.length;i++){
        if (!(repStr[i]>=0 && repStr[i]<= 255)){
            return [];
        }
    }
    return repStr;
}

module.exports= String2;